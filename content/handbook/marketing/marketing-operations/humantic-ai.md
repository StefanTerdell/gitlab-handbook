---
title: "Humantic AI"
---

**Page under construction**

Humantic AI helps salespeople communicate effectively with their buyers by combining the best of Generative AI and DISC Selling.

We are currently using the light mode version that includes Humantic AI Chrome Extension, Dashboard, Email Personalization, and Calendar Integration.

Humantic AI is compatible with LinkedIn, Google Calendar and Groove.

To following along on the project, follow to epic: https://gitlab.com/groups/gitlab-com/marketing/-/epics/5310

## Access

Humantic AI will be made available to AMER BDRs and a couple SDRs. The platform is limited to English speakers as personalization in other languages is not currently offered.

## Integrations

1. [Groove](/handbook/marketing/marketing-operations/groove/index.html/) - personalization email copy
2. Google Calendar - personality insights on meeting attendees

## Support

You can drop any of your questions or concerns related to Humantic AI to the #mktgops Slack Channel.

## Setup Chrome Extension

1. Download the Humantic AI [chrome extension](https://chromewebstore.google.com/detail/humantic-ai/iklikkgplppchknjhfkmkjnnopomaifc?hl=en)
1. Go to a website that is compatible with Humantic AI, this includes LinkedIn, Google Calendar and Groove.
1. Sign in via Google, and allow Humantic to access your Google Account.

## Setup Google Calendar

1. Click on the Humantic AI [chrome extension](https://chromewebstore.google.com/detail/humantic-ai/iklikkgplppchknjhfkmkjnnopomaifc?hl=en)
2. Click to the hamburger menu > `Settings` > `Manage Calendar`
3. Enable `Calendar Access`
4. Sign in via Google account

## Use Cases by Platform

There are several use case available for Humantic. Humantic AI requires the email address and an active LinkedIn profile to predict the personality type of the prospect.

### LinkedIn

#### Research Personality via LinkedIn

1. Go to LinkedIn.com and look up your prospect's profile.
2. Select `Know Personality` from the chrome extension.
3. Scroll through their Humantic AI profile to understand their buyer personality.

#### Personalized Connection Requests

1. When you are ready to contact the customer, go to the prospect's profile.
2. Click on `Connect` > `Add a Note`.
3. Input your text and select the Humantic AI to personalize your message.

#### LinkedIn InMail Personalization

1. Go to the prospect's profile (ensure their profile allows for InMail).
2. Click on `Message`.
3. In the InMail will pop up, input your text and select Humantic to personailize your message.

### Google Calendar

When Google Calendar is [configured with Humantic AI](/handbook/marketing/marketing-operations/humantic-ai/#setup-google-calendar), you'll receive an email provide buyer and personality insights of all meeting attendees 1hr to 30 min prior to a scheduled meeting.

### Groove

1. Go to the Action page.
1. Pick a prospect with an email action.
1. Click on the action icon and make sure the Groove omnibar is open.
1. Check on the Humantic AI chrome browser for their personality details.
1. When you are ready, select the `Personalize` button on the bottom left of the email editor.
1. Click on the check mark to accept the personalize email copy.
1. Edit the copy as needed.
1. And send!

## DISC Selling

DISC Selling is a sales methodology where salespeople identify their
buyer's DISC personality type, and accordingly modify their sales
approach to suit their preferences.

D - People with a Dominant personality like to lead and take charge of
situations. When you're selling to them, be direct and to the point.

I - People with an Influential personality like to be around people. When you're selling to them, be friendly and enthusiastic.

S - People with a Steady personality like to avoid risk and conflict. When you're selling to them, slow down, be calm and reassuring.

C - People with a Calculative personality like to be detail-oriented. When selling to them, focus on providing details and removing doubts.
