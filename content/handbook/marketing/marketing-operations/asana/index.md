---
title: "Asana"
description: "Asana is a collaborative work management platform that will support GitLab's mission to enable everyone to contribute to and co-create the software that powers our world. GitLab's Marketing team is planning to use Asana to track projects (e.g., Product Launches), connect work to goals, and coordinate work across the team. Asana will have access to Orange data including pre-release product launch information and GitLab issues and epics in private projects."
---

## About Asana

[Asana](https://app.asana.com/) is a collaborative work management platform that will support GitLab's mission to enable everyone to contribute to and co-create the software that powers our world. GitLab's Marketing team is planning to use Asana to track projects (e.g., Product Launches), connect work to goals, and coordinate work across the team.

## Why?

Our team is using Asana to add a made-for-marketing project and task management tool so that we can streamline and simplify projects and processes, improve efficiency, collaboration, and transparency, and free up time for our team to better understand how our customers use GitLab.

We've consistently heard from team members across the marketing org that there is an opportunity to improve efficiency. The main problems we are looking to solve are:

- Stretching GitLab to perform as a project management tool for Marketing processes it was not built for, and use cases it will never be used for by our customer base.
- This can cause undue administrative work, bottlenecks and detracts from our day-to-day commitment to efficiency.
- Adding a made-for-marketing project and task management tool will improve efficiency, freeing up time for our team to learn about and understand how our customers use GitLab

## Users

Asana licenses will be rolled out across the entire Marketing Org, excluding Sales Development and the Data Team.

View Only licenses are available to GitLab team members. 

## Asana Implementation Project

The Marketing Ops team started implementation on 2024-07-12. We are managing the [implementation project in Asana](https://app.asana.com/0/1207801099246898/1207801099246898).

## Getting Started in Asana

### Tasks

Tasks are the basic unit of action in Asana. Tasks are most similar to GitLab issues. You can create new tasks, duplicate an existing task, merge two tasks together, create subtasks, or delete a task. Create a task if you have a smaller effort that logically fits into an existing Project.

Asana is most useful when individuals can contribute ideas and move action items forward. Tasks are the foundational work items within Asana. Task names should be specific, clear, and action-based. Use a verb where possible. For example, instead of titling a task “Blog post,” title it “Write [title] blog post” and create a second task called “Publish [title] blog post.”

#### Tasks Descriptions

A task description should give the assignee all the necessary information to complete the task. Here are some tips:

- Link to relevant work in the task description by `@mentioning` related people and hyperlinking relevant tasks, projects, or messages.
- Add links and attachments to include external work, centralizing information.
- Mark a task as dependent upon another, so teammates start that task when the prior task is completed.
- Use rich text in task descriptions to clarify your message with formatted text and lists.

#### Assigning Tasks

Every task in Asana can have only one assignee, who serves as the Directly Responsible Individual (DRI). This person is accountable for ensuring the task is completed. However, tasks often require collaboration with multiple team members. Here's how to effectively manage task ownership and collaboration:

##### DRI (Task Asignee) Guidelines

The DRI should be:

- The person primarily responsible for the outcome of the work
- Someone with the authority to make decisions about the task
- Available during the task's timeline

##### Collaboration Structure

Task collaborators (followers) should include:

- Stakeholders who need to stay informed
- Team members providing input or review
- People who need visibility into the task's progress
- Project managers overseeing the work

To add collaborators:

1. Click the "+" button in the task's collaborators field
1. Search for and select team members
1. Or use @mentions in comments to automatically add collaborators

##### When to Use Subtasks for Team Collaboration

When you assign a subtask, be sure the assignee has enough context from the parent task or within the subtask description. Avoid burying subtasks under too many layers. You can always convert subtasks to tasks.

Create subtasks when:

- Different team members are responsible for distinct pieces of work
- A task requires multiple sequential steps with different owners
- You need to track individual contributions within a larger task

Example structure:

Main Task: Q4 Blog Post Launch [DRI: Content Manager]
  └─ Subtask 1: Draft content [DRI: Writer]
  └─ Subtask 2: Design graphics [DRI: Designer]
  └─ Subtask 3: SEO review [DRI: SEO Specialist]
  └─ Subtask 4: Final approval [DRI: Content Manager]

#### Task Best Practices

- Provide clear context in the task name and description
- Provide a realistic and reasonable due date. As a team, you can decide how due dates are changed. Communicate in the comments of a task to indicate if a due date is flexible, or to re-negotiate a due date if needed.
- Hypertext relevant tasks or projects by @mentioning a person, project, task, or team.
- Add task collaborators to keep your team informed.

### Projects

Projects are used to organize and track all of the steps that must completed for a process or initiative. A project helps you map out the work needed to complete an initiative, maintain a process, or hit a goal. Projects are most similar to GitLab Epics.

Create a project if you have a large effort (10+ tasks) that involve a subset of an existing Team or the whole Team.

#### Creating and naming new projects

Projects allow you to organize all tasks related to a specific initiative, goal, or significant work in one place. Similar to tasks, anyone can create a project.

There are three general types of projects and associated naming conventions. Naming conventions can help keep your tasks and projects organized and help your team find information more quickly. When creating a new project, first decide what type of project you are creating.

1. Deadline-bound projects have a clear start and end date and clear exit criteria
   a. [FYXX] - [Subteam Name] - [Concise Project Name] 
      i. FY25 - Product Marketing - GitLab Duo Launch Plan
      ii. FY25 - Content - Blog Post - Enterprise Agile Planning 
2. Ongoing/Operational Processes represent an ongoing process with no specific end. Work moves through a repeatable set of stages
   a. OP - Calendar - Events
   b. OP - Calendar - Email
   c. OP - Intake - Marketing Ops
3. Reference Projects are a way to capture and organize information. These projects do not contain any actionable work.
   a. REF - Asana Naming Conventions
   b. REF - Events - AMER - Preferred Vendor List

#### Project Templates

Create a custom template or use an Asana created template to standardize common workflows and projects. Templates help get projects off to a quick start and ensure you haven’t missed any vital steps.

## When to Use Asana vs Other Tools

### Quick Decision Guide

Before starting your work, ask yourself:

- Is this Marketing Division work that involves multiple stakeholders? → Use Asana
- Is this a code change or technical documentation? → Use GitLab
- Do you need immediate, real-time communication? → Use Slack

### Detailed Breakdown by Tool

#### Asana: Project & Task Management

**Best for:**

- Marketing campaign planning and execution
- Cross-functional collaboration within Marketing
- Project tracking and status updates
- Action items and next steps
- Task delegation and progress tracking

#### Slack: Real-time Communication

**Best for:**

- Quick questions
- Real-time collaboration
- Team announcements
- Informal discussions

#### GitLab: Technical Work, Documentation, and Collaborating Outside of Marketing

**Best for:**

- Handbook updates
- Code changes / merge requests
- Opening issues to request support from teams outside of Marketing

### Cross-Tool Workflows

#### Asana + GitLab

1. Create the main project in Asana
2. Create relevant GitLab issues
3. Link the GitLab issues in your Asana tasks (in task description or use a custom field)
4. Update status in Asana, technical details in GitLab
5. Document important decisions or updates from the issue in Asana

#### Asana + Slack

1. Keep all project details and discussions about the project in Asana
2. Use Slack for time-sensitive updates only, or for cross-posting links to important Asana updates
3. Document important Slack decisions in Asana
4. Share Asana task links in Slack when needed

#### Example of Cross-Tool Workflow

##### Website Update Project

**Asana:**

- Main project management
- Timeline tracking
- Stakeholder updates
- Content approvals

**GitLab:**

- Merge requests
- Technical documentation
- Implementation details

**Slack:**

- Urgent deployment questions
- Quick status checks

#### Best Practices

**Do:**

- Create Asana tasks that link to relevant GitLab issues
- Move Slack discussions to Asana tasks when they become actionable items
- Use Asana for all project-related communications and updates
- Document important Slack and/or GitLab decisions in Asana

**Don't:**

- Create duplicate tracking systems across platforms
- Have lengthy project discussions in Slack
- Use GitLab for marketing task management
- Keep project documentation solely in Slack

#### Need Help Deciding?

If you're unsure which tool to use:

1. Consider who needs to be involved
2. Think about the type of work being done
3. Assess the timeline and urgency
4. When in doubt, start in Asana and link to other tools as needed

**Remember:** The goal is to keep related work together while using each tool for its strengths. When in doubt, ask yourself "Where would others look for this information?"

## How to get help with Asana

If you have questions, first self-service by reviewing this Handbook page, [Asana Help Center](https://help.asana.com/), and/or [Asana Academy](https://academy.asana.com/). If you cannot find a solution on your own, feel free to let us know using this [GitLab Internal Support Form](https://form.asana.com/?k=rIdwTWezTYqZhSI9vgJZsQ&d=306855239930259).

## Self-Paced Learning

The [Asana Academy](https://academy.asana.com/) is a great resource with live and on-demand webinars and workshops.
