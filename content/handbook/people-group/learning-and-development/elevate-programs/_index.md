---
title: Elevate Programs
---

Use the links below to navigate to each Elevate program. Here is a useful guide to ensure you're choosing the right program:

| Program Name | Description |
| ----- | -------- |
| [Elevate](/handbook/people-group/learning-and-development/elevate-programs/elevate/) | The next round of Elevate will being in January 2025, if you've been enrolled, please navigate here. |
| [Elevate Applied](/handbook/people-group/learning-and-development/elevate-programs/elevate-applied/) | Continuous learning and resources for those who've earned their Elevate certification |
| [Elevate+](/handbook/people-group/learning-and-development/elevate-programs/elevate+/) | Round 3 Ongoing, Round 4 will begin in Q1 FY26 |

**Not a GitLab People Leader?** We've put together a Level Up course available to all GitLab Team Members, to share the self-paced training content for each Elevate module. You can check it out here: [Elevate Learning Materials](https://levelup.gitlab.com/access/saml/login/internal-team-members?returnTo=https://levelup.gitlab.com/learn/learning-path/elevate-learning-materials).

#### Elevate Feedback Loop

To remain ahead of the curve, GitLab Talent teams will utilize HPT (High Performing Teams) workshop data, Elevate program feedback, engagement survey data, and direct input from executive leadership to continuously up-level program content. Just as GitLab integrates customer insights into product development, the same holds true for Elevate design and future iterations. Similar product [sensing mechanisms](/handbook/product/product-processes/sensing-mechanisms/) are used to guide continuous program improvement.
