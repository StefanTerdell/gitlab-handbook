---
title: "Executive Business Reviews (EBRs)"
---

View the [CSM Handbook homepage](/handbook/customer-success/csm/) for additional CSM-related handbook pages.

---

## Overview

An **Executive ROI Progress and Impact Review (EPR)** is a strategic meeting with stakeholders from both GitLab and the customer. It is an extension of the account planning process and part of the CSM’s regular business rhythm. The Executive ROI Progress and Impact Review aims to demonstrate to the customer leadership and stakeholders the value they are receiving in their partnership with GitLab.

The EPR is interactive, with discussions on the customer’s desired business outcomes, ROI metrics, progress against these metrics, and alignment on strategic next steps. The key element of an EPR is providing customer stakeholders the opportunity to speak on what matters to them and creating a success plan to achieve their business goals.

## Alignment with Stakeholders: The Importance of the EPR

The Executive ROI Progress and Impact Review (EPR) is an essential opportunity to align on strategic goals, assess progress, and plan for future success. It typically brings together both GitLab and customer leadership to ensure mutual understanding and continued collaboration. During regular meetings, introduce the EPR to key customer stakeholders, highlighting its value in driving business outcomes and fostering stronger partnerships.

GitLab encourages participation from decision-makers and influential stakeholders, as their insights and feedback are crucial to a productive EPR. Scheduling can take time, so initiating this discussion at least three months in advance is recommended to ensure availability. After the conversation, provide a follow-up summary outlining the EPR’s purpose and key scheduling details.

## Stakeholder Recommendations

### Customer Stakeholders that would benefit from an Executive ROI Progress and Impact Review

| Customer Stakeholder Recommendations                | GitLab Potential Attendance            |
|-----------------------------------------------------|----------------------------------------|
| Application Security (for example, SVP, VP, Director)      | Customer Success & Leadership          |
| Engineering Leadership (for example, SVP, VP, Director)    | Sales & Leadership                     |
| Site Reliability Engineering Leadership (for example, SVP, VP, Director) | Engineering & Leadership |
| Chief Technology Officer                            | Product Management & Leadership        |
| Chief Information Security Officer                  | Solution Architects & Leadership       |
| Chief Product Officer                               |                                        |
| DevOps Leadership (for example, SVP, VP, Director)         |                                        |
| Product Leadership (for example, SVP, VP, Director)        |                                        |

## Executive ROI Progress and Impact Review (EPR) Process and Overview

This section outlines the EPR process, highlighting key stages: preparation, delivery, and follow-ups. While the process can be tailored to meet specific customer needs, this guide provides a typical flow to ensure strategic alignment, seamless execution, and timely action on key outcomes.

1. **Internal Alignment (3-4 months prior)**: CSM and the account team set goals, timing, and participants to ensure a successful EPR.
2. **Initial Customer Discussion (3-4 months prior)**: CSM engages with the customer to define focus areas and align attendees.
3. **Scheduling (2-3 months prior)**: Confirm date and attendees, and secure participation from GitLab leadership.
4. **EPR Planning (1 month prior)**: Build the EPR content, aligning with internal and customer stakeholders to ensure strategic focus.
5. **Internal Review (3-4 weeks prior)**: Finalize the presentation with internal GitLab teams and adjust based on feedback.
6. **Customer Review (2-3 weeks prior)**: Validate the agenda and content with the customer to ensure alignment with their expectations.
7. **Final Prep (2 weeks prior)**: All GitLab participants align on objectives and final details for the EPR.
8. **Delivery**: The EPR is conducted, focusing on strategic alignment and driving outcomes for the customer.
9. **Follow-ups (Within 1 business day)**: A follow-up email is sent to review action items, ensuring timely completion. The Success Plan is also updated to reflect insights from the EPR.

### Final Review with the Customer

Throughout the EPR’s development, the CSM & account team collaborate with their customer contacts. A final review of the EPR is conducted with the account team and customer contacts to ensure content & talk track align with expectations and are properly structured. Proactive communication is coordinated to keep the EPR planning on track and include all necessary participants.

## Sample Agenda: Reviewing ROI, Impact Analysis, and Future Goals

The EPR session focuses on evaluating the customer’s GitLab adoption ROI, its impact on their business, and alignment on future goals. It provides an opportunity to review progress, address challenges, and plan next steps to ensure ongoing value and strategic alignment.

**Sample Agenda:**

1. **Welcome and Introductions** – Overview of key attendees and meeting objectives.
2. **Customer Journey** – Overview of the reason for purchase and GitLab enablement/ROI journey.
3. **ROI & Value Review** – Assess the measurable impact of GitLab solutions on business outcomes.
4. **Adoption & Success Metrics** – Review current adoption progress and identify areas for improvement.
5. **Future Goals & Roadmap Alignment** – Discuss upcoming initiatives, long-term goals, and how GitLab can support them.
6. **Action Items and Next Steps** – Confirm key takeaways and follow-up actions to drive future success.

### Additional Conversation Points

These questions facilitate discussion and help uncover key business objectives:

- What are your largest current initiatives and areas of focus?
- Looking ahead, what are your top 2-3 priorities for the coming year?
- What significant changes have occurred since last year?
- Where do you envision your organization in the next few years? What are your long-term goals?
- Regarding security, how does your team handle the triage and resolution of issues? Who is involved, and how does this impact cycle time?
- How are security vulnerabilities affecting your development processes and timelines?
- Can you walk us through your cloud and app modernization strategy?
- What visibility do your leaders have into progress, and how are you gathering this information?
- For initiatives without clear metrics & due dates: What does success look like for this effort? How will you measure progress and outcomes?

## EPR Length

The recommended EPR length is typically 90 minutes; however, this is flexible based on customer needs and content. An EPR should provide time for key focus areas and customer objectives, ensuring maximum benefit to all involved.

## EPR Location: Maximizing Value through Flexibility

EPRs can be flexible to online or onsite. An in-person EPR is ideal to maximize interaction and discussion among participants. Additional sessions should be scheduled around the EPR itself, such as:

- Enablement sessions aligned with ongoing adoption efforts or future adoption plans.
- Meetings with customer stakeholders for GitLab use cases to align on use case adoption & value.

## Follow-ups

### Summary Email

Within 1 business day, the CSM sends a follow-up email thanking participants, summarizing key takeaways, action items, and next steps, with assigned responsibilities and deadlines. The email will also include the slide deck and any agreed-upon materials.

### Action Items

Action items and next steps, captured in the EPR deck and discussion, must be promptly addressed. The CSM ensures follow-ups are completed by the responsible parties to maintain momentum.

### Success Planning

No EPR is complete without updating the success plan. Within a few days of the EPR, while information is fresh, the CSM updates the customer’s success plan with the confirmed objectives and action items. All sections of the success plan should be updated based on the EPR discussion & takeaways.

## FAQ

**Q: Is this EPR just another sales pitch for renewal?**
A: No, it is not. The EPR is a strategic touchpoint focused on aligning GitLab with the customer’s organizational objectives, not a renewal conversation. Renewal discussions are separate and follow a different process.

**Q: Who should attend the EPR from the customer side?**
A: Key stakeholders who influence or oversee strategic initiatives should attend, ensuring the session aligns GitLab solutions with long-term objectives.

**Q: How is an EPR different from regular cadence calls?**
A: Cadence calls are often project-focused, whereas an EPR provides a strategic review of long-term goals, current successes, and future roadmaps, involving leadership from both sides.

**Q: How are feature requests handled during the EPR?**
A: The EPR focuses on strategic goals, but notable feature requests aligning with objectives may be acknowledged. In-depth feature discussions are usually handled separately.

**Q: What kind of preparation should a customer do before the EPR?**
A: Reviewing GitLab usage and identifying upcoming initiatives or goals helps ensure a productive discussion on how GitLab can best support objectives.

---
*For further details, visit:* [GitLab Success Plans Documentation](/handbook/customer-success/csm/success-plans/)
