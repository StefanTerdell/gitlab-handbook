---
title: Cells Infrastructure Team
---

## Vision

The Cells Infrastructure team is responsible for developing key services and components of the Cells architecture. The team also collaborates closely with other infrastructure and development teams on their contributions to Cells.

## Team Members

{{< team-by-manager-slug manager="nick-nguyen" team="Cells Infrastructure" >}}

## Resources

* Slack (internal): [#g_cells_infrastructure](https://gitlab.enterprise.slack.com/archives/C07URAK4J59), [#g_cells_infrastructure_standup](https://gitlab.enterprise.slack.com/archives/C07UWPM2Y0P)
