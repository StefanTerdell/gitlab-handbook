---
title: "ESG Training"
description: "ESG Training for GitLab team members"
---

## What is ESG?

ESG stands for Environmental, Social and Governance and is a framework designed to be embedded into an organization’s strategy and considers the ways in which the organization generates value for all of its stakeholders such as team members, customers, suppliers, and investors. An ESG strategy identifies which environmental, social and governance issues are most important to the company and its stakeholders and informs how the organization can minimize related risks and seize related opportunities to create shared value.

Examples of ESG Issues:

- Environmental: The 'E' takes into account a company's use of natural resources and the impact of the company's operations on the environment, both in its direct operations and its supply chain. Examples of specific environmental issues include greenhouse gas emissions, energy use, water use, waste management and the environmental impact of the company's products and services. As a fully remote software company without owned and operated facilities or a physical product, GitLab’s environmental footprint is relatively small, but is growing. GitLab conducts an annual greenhouse gas inventory that measures the company’s indirect emissions (Scope 3 emissions) that come from our supply chain, business travel, remote work, and investments.

- Social: The 'S' takes into account how a company manages its relationships with its employees and the communities in which it operates, both in its direct operations and its supply chain. Examples of specific social issues include diversity of its workforce and its suppliers, employee work/life balance, employee pay equity, talent management and engagement, community engagement and the social impact of the company's products and services.

- Governance: The 'G' takes into account the governance structures set up to manage ESG-related risks and opportunities. Specific examples of governance issues include C-suite oversight and accountability for ESG related risks and opportunities, Board of Director independence, skill sets and experience, board diversity, compensation practices, board oversight of ESG practices, cybersecurity and data privacy and business ethics/compliance.

## Why ESG Matters to GitLab

By ensuring that good ESG practices are embedded into the organization, it will help us advance our mission to make it so that everyone can contribute. ESG allows us to understand what our stakeholders care about, meet their expectations and to make progress to advance social and environmental goals while minimizing risk and creating value for the organization.

- Meet and Exceed Customer Expectations & Remain Easy to Transact With
  - GitLab’s customers have ESG commitments and policies that extend to the companies they do business with, like GitLab.
  - Many of our customers are under pressure to comply with new regulation and meet their own ESG goals. By having a strong ESG program, GitLab is helping its customers achieve their goals and comply with new regulations.
 
- Comply with New Regulation
  - Starting in 2026, GitLab is required to comply with two new ESG regulations in the US and the EU. These regulations require comprehensive and detailed disclosure covering a wide range of ESG topics (e.g., climate change, working conditions, human rights, business ethics).

- Gain a Competitive Advantage
  - As sustainability, particularly climate action, becomes a growing concern for businesses globally, companies with strong ESG practices are better positioned to differentiate themselves and gain a competitive advantage.

- Attract Investors
  - [Over 60% of U.S. investors](https://www.statista.com/statistics/1480884/share-of-investors-willing-to-pay-an-esg-premium-us/?utm_source=chatgpt.com) surveyed were willing to pay a premium when investing in companies aligned with ESG priorities. Approximately 40% of these investors were willing to pay a premium ranging from 1% to 5%, while nearly 20% were willing to pay between 6% and 10%.
  - [93% of U.S. investors](https://www.edelman.com/sites/g/files/aatuss191/files/2020-11/Edelman%202020%20Institutional%20Investor%20Trust_0.pdf?utm_campaign=Friends%20of%20Dave&utm_medium=email&utm_source=Revue%20newsletter) expect the companies they invest in to increase their prioritization of ESG initiatives.

- Recruit & Retain Team Members
  - [42% of employees](https://cdn.sanity.io/files/umko2xz8/production/37643e7e8b837c22e1337badb8c79541f1807e5a.pdf) would leave their current employer for one that is making a greater impact on the world.
  - [69% of employees](https://www2.deloitte.com/us/en/insights/environmental-social-governance/importance-of-sustainability-to-employees.html?utm_source=chatgpt.com) want their employers to take action on climate by reducing emissions and sourcing renewable energy. The sentiment is higher amongst employees ages 18-34.

## ESG at GitLab

### ESG Team

- The [ESG Team](/job-families/legal-and-corporate-affairs/environmental-social-governance/) is part of Legal and Corporate Affairs. The ESG Team creates and maintains GitLab’s Corporate Sustainability strategy and programs. This includes ESG disclosures and public ESG reporting, identifying and prioritizing key issues to advance GitLab’s social and environmental goals, and creating partnerships with nonprofit organizations that support GitLab’s values and mission.

### ESG Programs

- Compliance & Reporting
  - GitLab is subject to new regulations in the EU and US that will require us to disclose a wide range of quantitative and qualitative ESG information in 2026. To date, GitLab has published two voluntary annual ESG reports, providing a strong foundation for complying with the new ESG reporting regulations.

- Climate Action
  - Climate is quickly becoming a key component of corporate responsibility, and with it comes new expectations from customers, investors, regulators and team members. 85% of our top 20 customers have greenhouse gas reduction targets. To remain easy to transact with, GitLab will need to meet the expectations of our customers by taking action on climate change.

- GitLab for Non-Profits
  - GitLab launched GitLab for Nonprofits, an in-kind donation program in 2023. Through this program, GitLab supports Registered 501c3 (or jurisdictional equivalent) Nonprofit Organizations in good standing that align with our values by offering free licenses and seats.

- GiveLab
  - GiveLab is GitLab’s team member volunteer program. Volunteerism is an effective way to build trust through social connections - this leads to higher individual and team motivation, greater cross-functional collaboration.

### GitLab's ESG Materiality Assessment

- GitLab completed its first [double materiality assessment](/handbook/legal/esg/#esg-materiality-matrix) in January 2023. The assessment considered the perspectives of all of GitLab's stakeholders including team members, senior leadership, customers, community members, board members, investors. The assessment involved speaking with GitLab's stakeholders to understand the key topics they believe have the biggest impact on the success of the business and where GitLab has the biggest impact on the environment, society and people. We also considered ESG regulation, our peers, competitors and nonprofit organizations.The materiality assessment was the first step in developing the company's first ESG strategy.

### GitLab's 6 Key Topcs

- Environment
  - Greenhouse gas emissions
    - Measurement and reduction of scope 1, 2 and 3 emissions
    - Greenhouse gas reduction targets, goals and commitments
- Social
  - Diversity, Inclusion and Belonging (DIB)
    - Diverse hiring and recruitment of underrepresented groups
    - Culture of DIB (events, TMRGs, courses, etc.)
    - Pay and promotion equity and inclusive benefits
    - Board, leadership and workforce diversity KPIs
    - Product accessibility
  - Talent Management and Engagement
    - Talent attraction and recruitment
    - Team member learning and development
    - Leadership programs
    - Succession planning
    - Talent retention
    - Workplace culture and remote work
    - Comp and benefits
- Governance
  - Information Security and Data Privacy
    - Data, system and network breaches
    - Monitoring of emerging threats
    - Information security training
    - Customer data use transparency
    - Data processing and storage
    - PII
  - Responsible Product Development
    - Open source
    - Ethical AI
    - Human rights issues from product use
    - Environmental impact considerations for product
    - Data processing and storage
  - Business Ethics
    - Up to date code of conduct, anti-corruption, anti-bribery policies
    - Regulatory compliance
    - Legal proceedings and monetary losses
    - Internal compliance
    - Oversight and ethics training
    - Human rights policies, risk assessments and due diligence

- GitLab’s second materiality assessment will be completed in Q1 2025 and will be in alignment with the requirements of CSRD.

### GitLab's ESG Disclosures

- Every year, GitLab publishes an ESG report in the Handbook, which will describe key accomplishments for the year, include descriptions of how we manage relevant ESG risks and opportunities, highlight new programs and policies and report metrics across our material ESG topics to hold ourselves accountable year over year for improving our performance.

### ESG Regulatory Environment

- Over the past 12 months, the EU, U.S., Australia, and Canada have introduced ESG regulations that require many companies to disclose ESG information alongside their financial reports. Many of these regulations apply to GitLab, and many of our customers will also need to comply by providing ESG data on their suppliers, such as GitLab.

## Responding to Customer Questions about ESG

- If a customer or potential customer has questions related to GitLab’s ESG practices as part of an RFP, please follow the [RFP process](/handbook/security/security-assurance/field-security/field-security-rfp/).
- If a customer or potential customer requests information via an ESG questionnaire, please [open an issue here](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/new) and select the ‘ESG Questionnaire’ template.’
- GitLab participates in EcoVadis and customers can request our scorecard via the EcoVadis platform
- GitLab also participates in Integrity Next. If you receive a request from a customer to complete an Integrity Next questionnaire, please do not create a new profile. Instead, please forward the request to the ESG team, so we can complete the questions. This avoids having multiple profiles under GitLab Inc.

## Responding to Investor Questions about ESG

- If an investor contacts you regarding ESG, please advise them to reach out via the [IR page](https://ir.gitlab.com/ir-resources/contact-ir).

## Where to Find More Information

- Review the [ESG Handbook Page](/handbook/legal/esg/) to learn more about GitLab’s current progress on ESG initiatives
- Read GitLab’s [latest ESG Report](/handbook/company/esg/)
- Join the #ESG slack channel to follow along for updates and ask questions
