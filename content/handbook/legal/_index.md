---
title: Legal & Corporate Affairs ("LACA")
---

## Our Mission

We leverage our versatile skill sets to drive positive outcomes for GitLab and our customers. Approachable and collaborative, LACA team members are trusted thought partners to our colleagues across GitLab, and facilitate efficient and informed decision making in all areas of the business.

**In short: LACA – We’re your one phone call.**

## Legal & Corporate Affairs Functional Groups

### Commercial

The [Commercial](https://handbook.gitlab.com/handbook/legal/commercial/) group supports GitLab’s go-to-market and procurement functions with knowledge and creativity in advising on contractual and regulatory matters to facilitate customer-centric results in accordance with the Company’s values.

### Corporate & Compliance

The [Corporate & Compliance](/handbook/legal/publiccompanyresources/) group drives strategic results for GitLab via cross-functional relationships to support a climate of principled corporate governance and the company’s culture of compliance.

### Corporate Development

The [Corporate Development](/handbook/acquisitions/) group conducts diligent and thorough analysis to strategically identify, evaluate and execute potential acquisitions to accelerate the product roadmap and advance the company’s competitive position in the market.

### Employment

The [Employment](/handbook/legal/employment-law/) group provides thoughtful, pragmatic recommendations to address complicated employment situations within the legal confines of the many jurisdictions in which GitLab team members reside.

### Environment, Social, and Governance (ESG)

The [ESG](/handbook/legal/esg/) group works cross-functionally to integrate ESG considerations, ESG regulatory compliance, and responsible business practices as part of GitLab’s overarching mission to make our customers successful.

### Privacy and Intellectual Property

The [Privacy](/handbook/legal/privacy/) [and Intellectual Property](/handbook/legal/product/) group enables fast-paced product development within applicable regulatory confines; privacy solutions so customers can confidently co-create in those products; and robust protection of Company IP.

### Risk Management and Dispute Resolution

The [Risk Management and Dispute Resolution](/handbook/legal/risk-management-dispute-resolution/) group directs GitLab’s unified, transparent and ethical approach to defending company interests while resolving disputes should they arise.

### Strategy and Legal Operations

The [Strategy and Legal Operations](/handbook/legal/legalops/) group promotes continued process improvement such that LACA may be efficient and effective in its performance, including procurement of the most suitable tools and technology.

## How to Reach Us

For quick questions that ***do not*** require legal advice, deliverables, or any discussion of confidential information, you can reach out to the GitLab Legal and Corporate Affairs Team in Slack at [#legal](https://gitlab.slack.com/archives/legal). We find this channel best for questions regarding process, who handles what, or how to find certain things if the handbook has not yielded the right result for you after searching. #legal is not a private channel, so your inquiry will be visible to the entire company. One of our Team Members will do their best to answer your question in a timely fashion.

To open a general Legal Issue for questions related to deliverables and non-sensitive information, use [this template](https://gitlab.com/gitlab-com/legal-and-compliance/-/issues/new?issuable_template=general-legal-template). See the [Legal Issue Tracker Workflow](/handbook/legal/issue-tracker-workflows/) for more information.

For sensitive, private, or confidential requests, email [legal_internal@gitlab.com](mailto:legal_internal@gitlab.com).

## Anonymous Internal Ethics and Compliance Reporting

We take employee concerns very seriously and encourage all GitLab Team Members to report any ethics and/or compliance violations by using [EthicsPoint](https://secure.ethicspoint.com/domain/media/en/gui/74686/index.html). Further details can be found in the [People Group Handbook](/handbook/people-group/) under [How to Report Violations](/handbook/people-group/#how-to-report-violations) and in our [Code of Business Conduct and Ethics](/handbook/legal/gitlab-code-of-business-conduct-and-ethics/).

## GitLab Policies

- [Anti-Corruption Policy](/handbook/legal/anti-corruption-policy/)
- [Anti-Fraud Policy](/handbook/legal/anti-fraud-policy/)
- [Anti-Harassment Policy](/handbook/people-group/anti-harassment/)
- [Anti-Retaliation Policy](/handbook/legal/anti-retaliation-policy/)
- [Code of Business Conduct and Ethics](https://s204.q4cdn.com/984476563/files/doc_downloads/govdoc/GitLab-Code-of-Business-Conduct-Ethics-2024-06-25.pdf)
- [Corporate Communication Policy](/handbook/legal/corporate-communications/)
- [Employee Privacy Policy](/handbook/legal/privacy/employee-privacy-policy/)
- [GitLab Terms of Service](https://about.gitlab.com/terms/)
- [Insider Trading Policy](https://drive.google.com/file/d/184tkDZaPmZAxwQRQ5BwBv2JpfTWbWmBW/view?usp=sharing)*
- [Internal Acceptable Use Policy](/handbook/people-group/acceptable-use-policy/)
- [Modern Slavery Act Transparency Statement](/handbook/legal/modern-slavery-act-transparency-statement/)
- [Partner Code of Ethics](/handbook/legal/partner-code-of-ethics/)
- [Philanthropy Policy](/handbook/legal/philanthropy-policy/)
- [Privacy Policy](https://about.gitlab.com/privacy/)
- [Records Retention Policy](/handbook/legal/record-retention-policy/)
- [Related Party Transactions Policy](/handbook/legal/gitlab-related-party-transactions-policy/)
- [Social Media Policy](/handbook/marketing/team-member-social-media-policy/)
- [Whistleblower Policy](https://drive.google.com/drive/folders/1kB3k5FRnR3OUBP0Eyo3SxxyPKeiRFfUk)*
- [Public Sector Code of Ethics](https://drive.google.com/file/d/1whEuxvIwy3M33N3vbUUkGrOKvkow5LAw/view?usp=share_link)

*Only available to GitLab Team Members.

## Authorization Matrix

The [Authorization Matrix](/handbook/finance/authorization-matrix/#authorization-matrix) designates who is authorized to sign legal documents. Only GitLab Team Members with signature authority can execute agreements on behalf of GitLab.

## Non-Disclosure Agreements

Follow the [Non-Disclosure Agreement Process](/handbook/legal/nda/) to learn how to send an NDA in DocuSign or request an NDA if you do not have DocuSign access.

Note that this process is only for standard GitLab NDAs. In the event a non-standard NDA is needed, follow the steps [here](/handbook/legal/nda/#non-standard-nda-requests).

## General Legal FAQs

### Legal Holds

{{% details summary="What is a legal hold?" %}}
A legal hold is the process GitLab uses to preserve all forms of relevant evidence, whether it be emails, instant messages, physical documents, handwritten or typed notes, voicemails, raw data, backup tapes, and any other type of information that could be relevant to an investigation, pending or imminent litigation or when litigation is reasonably anticipated. Legal holds are imperative in preventing spoliation (destruction, deletion, or alteration) of evidence which can have a severely negative impact on a company's case, including leading to sanctions. Once GitLab becomes aware of an investigation or potential litigation, a GitLab attorney will provide notice to the impacted team members, instructing them not to delete or destroy any information relating to the subject matter of the investigation or potential litigation. The legal hold applies to paper and electronic documents. During a legal hold, all retention policies must be overridden.
{{% /details %}}

### Freedom of Information Act Requests

{{% details summary="What is a Freedom of Information Act (FOIA) request?" %}}
The Freedom of Information Act ("FOIA") provides public access to all United States federal agency records except for those records (or portions of those records) that are protected from disclosure by any of [nine exemptions or three exclusions](https://www.dhs.gov/foia-exemptions) (reasons for which an agency may withhold records from a requestor). Occasionally the records of a federal agency under a FOIA request may include GitLab records in the possession of the agency (i.e. when the agency is a customer of GitLab). In such an event, the federal agency will notify GitLab of the FOIA request and provide GitLab with the documents that the federal agency intends to release in response to the FOIA request. A GitLab legal team member will review the list and content of the documents identified by the federal agency pursuant to the FOIA request and will provide the appropriate response and/or make redactions to those documents, as necessary, prior to their release.

In the event you receive a notification from a US federal agency pursuant to a FOIA request, indicating that GitLab documents or information have been identified for release by an agency , please immediately forward the request to FOIA@gitlab.com.
{{% /details %}}

### Foreign Corrupt Practices Act

{{% details summary="What is the Foreign Corrupt Practices Act?" %}}
The Foreign Corrupt Practices Act ("FCPA") is a United States federal law that prohibits U.S. citizens and entities from bribing foreign government officials to benefit their business interests. It is not only an invaluable tool to help fight corruption but one to which we must be compliant. As GitLab Inc. is a U.S. incorporated entity, we need to make sure our operations worldwide are compliant with the provisions of the Foreign Corrupt Practices Act. To that end, GitLab requires Team Members to complete an annual online course relating to anti-bribery and corruption at GitLab. In the training, learners will explore improper payments, including facilitation payments and personal safety payments, as well as policies on commercial bribery. The goal of the course is to ensure our Team Members understand what it takes to avoid corruption, especially in high-risk countries, and to ensure GitLab is compliant with legal and regulatory obligations.
{{% /details %}}

For additional General Legal FAQs, please refer to the [Internal Handbook](https://internal.gitlab.com/handbook/legal-and-compliance/#general-legal-faqs).
{.h2}
